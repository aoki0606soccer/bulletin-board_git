// alert("Hello!!");
//今日の日付データを変数に格納
//変数は"today"とする
// var today=new Date();
//
// //年・月・日・曜日を取得
// var year = today.getFullYear();
// var month = today.getMonth()+1;
// var week = today.getDay();
// var day = today.getDate();
//
// var week_ja= new Array("日","月","火","水","木","金","土");
//
// //年・月・日・曜日を書き出す
// var date = year+"年"+month+"月"+day+"日 "+week_ja[week]+"曜日";
//
// window.onload = function onLoad(date){
//     target = document.getElementById("date");
//     target.innerHTML = date;
// }
window.addEventListener('load', () => {
  var today=new Date();
  //年・月・日・曜日を取得
  var year = today.getFullYear();
  var month = today.getMonth()+1;
  var week = today.getDay();
  var day = today.getDate();
  var week_ja= new Array("日","月","火","水","木","金","土");
  // 現在時刻を表示する
  var nowTime = new Date(); //  現在日時を得る
var nowHour = nowTime.getHours(); // 時を抜き出す
var nowMin  = nowTime.getMinutes(); // 分を抜き出す
var nowSec  = nowTime.getSeconds(); // 秒を抜き出す
var msg =nowHour + ":" + nowMin + ":" + nowSec;
  //年・月・日・曜日を書き出す
  var dates = year+"年"+month+"月"+day+"日 "+week_ja[week]+"曜日";
  console.log(dates);
  target = document.getElementById("date");
  console.log(target);
  target.innerHTML = dates+msg;
});
